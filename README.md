# THE ZALDO ROGUE PROJECT

## A rogue-like zelda-like top-down adventure game

Scope général:
+ Générer une région procédurale: plaine, village, forêt
+ PNJ et dialogue
+ Déplacement et combat
+ Monstres errants
+ Rubis et Boutique
+ Générer un donjon procédural
+ (Générer un) combat de boss (procédural?)
+ Voler les graphismes de minishcap en attendant de faire les nôtres
+ Sauver la princesse
