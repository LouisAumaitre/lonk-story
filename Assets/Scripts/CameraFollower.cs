﻿using System.Collections;
using System.Collections.Generic;
using LonkStory.Storage;
using LonkStory.Utilities;
using UnityEngine;

namespace LonkStory
{
    public class CameraFollower : MonoBehaviour
    {
        public Transform target;

        void Update()
        {
            if (target != null)
                transform.SetXY(target.position);

            var halfHeight = Camera.main.orthographicSize;
            var halfWidth = 9f; // (halfHeight * 2) * 3 / 4;

            if (transform.position.y < halfHeight)
            {
                SetY(halfHeight);
            }
            if (transform.position.x < halfWidth)
            {
                SetX(halfWidth);
            }
            if (transform.position.y > Store.CurrentMap().height - halfHeight)
            {
                SetY(Store.CurrentMap().height - halfHeight);
            }
            if (transform.position.x > Store.CurrentMap().width - halfWidth)
            {
                SetX(Store.CurrentMap().width - halfWidth);
            }
        }

        private void SetX(float limit)
        {
            var nextPosition = transform.position;
            nextPosition.x = limit;
            transform.SetXY(nextPosition);
        }

        private void SetY(float limit)
        {
            var nextPosition = transform.position;
            nextPosition.y = limit;
            transform.SetXY(nextPosition);
        }
    }

}
