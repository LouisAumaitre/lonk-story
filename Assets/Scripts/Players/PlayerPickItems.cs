using LonkStory.Constants;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory
{

    public class PlayerPickItems : MonoBehaviour
    {
        private void _HandlePick(PickableItem item)
        {
            if (item.id == ItemId.HEART)
            {
                Store.player.life += 3;
            }
        }

        void OnTriggerEnter2D(Collider2D other)
        {
            var pickableItem = other.GetComponent<PickableItem>();
            if (pickableItem != null)
            {
                Destroy(pickableItem.gameObject);
                _HandlePick(pickableItem);
            }
        }
    }
}