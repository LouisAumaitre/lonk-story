﻿using System.Collections;
using System.Collections.Generic;
using LonkStory;
using UnityEngine;

public class PlayerSword : MonoBehaviour
{
    void OnTriggerEnter2D(Collider2D other)
    {
        var destroyable = other.GetComponent<DestroyableItem>();
        if (destroyable != null)
        {
            destroyable.GetDestroyed();
        }
    }
}
