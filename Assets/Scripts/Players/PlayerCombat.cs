﻿using System.Collections;
using System.Collections.Generic;
using LonkStory.Constants;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory.Players
{
    public class PlayerCombat : MonoBehaviour
    {
        // The attack GameObject is an attached child that represent the area of effect of the attack.
        // By default this GameObject is unactive until the player attack, then the attack GameObject
        // is active for the duration of the attack.
        public GameObject areaOfEffect;

        private float _attackDuration;
        private float _restDuration;

        void Start()
        {
            areaOfEffect.SetActive(false);
        }

        private void Attack(Direction dir)
        {
            _attackDuration = 0.5f;
            _restDuration = 1f;
            switch (dir)
            {
                case Direction.LEFT:
                    areaOfEffect.transform.localPosition = Vector2.left;
                    break;
                case Direction.RIGHT:
                    areaOfEffect.transform.localPosition = Vector2.right;
                    break;
                case Direction.BOTTOM:
                    areaOfEffect.transform.localPosition = Vector2.down;
                    break;
                case Direction.TOP:
                    areaOfEffect.transform.localPosition = Vector2.up;
                    break;
                default:
                    break;
            }
            areaOfEffect.SetActive(true);
            ActionManager.currentState = ActionState.Attack;
        }

        private void UpdateAttackInput()
        {
            if (ActionManager.AttackLeft)
            {
                Attack(Direction.LEFT);
            }
            else if (ActionManager.AttackRight)
            {
                Attack(Direction.RIGHT);
            }
            else if (ActionManager.AttackUp)
            {
                Attack(Direction.TOP);
            }
            else if (ActionManager.AttackDown)
            {
                Attack(Direction.BOTTOM);
            }
        }

        void Update()
        {
            if (_restDuration > 0)
            {
                _restDuration -= Time.deltaTime;
            }
            else
            {
                UpdateAttackInput();
            }

            if (_attackDuration > 0)
            {
                _attackDuration -= Time.deltaTime;
                if (_attackDuration < 0)
                {
                    areaOfEffect.SetActive(false);
                    ActionManager.currentState = ActionState.Game;
                }
            }
        }
    }
}
