﻿using System;
using System.Collections;
using System.Collections.Generic;
using LonkStory.Constants;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory.Players
{
    public class PlayerMovement : MonoBehaviour
    {
        public float speed = 16;

        private EventHelper _transitionEvent = new EventHelper(EventId.Transition);

        void Awake()
        {
            _transitionEvent.Subscribe(1, (sender, arg) =>
            {
                Teleporter tele = arg as Teleporter;
                if (tele == null)
                    return;
                Store.player.SetPosition(tele.destinationTile);
            });
        }

        void OnDestroy()
        {
            _transitionEvent.Unsubscribe();
        }

        void Update()
        {
            bool hasMoved = false;
            if (ActionManager.MoveUp)
            {
                hasMoved = true;
                Store.player.Move(Direction.TOP, speed * Time.deltaTime);
            }
            else if (ActionManager.MoveDown)
            {
                hasMoved = true;
                Store.player.Move(Direction.BOTTOM, speed * Time.deltaTime);
            }

            if (ActionManager.MoveLeft)
            {
                hasMoved = true;
                Store.player.Move(Direction.LEFT, speed * Time.deltaTime);
            }
            else if (ActionManager.MoveRight)
            {
                hasMoved = true;
                Store.player.Move(Direction.RIGHT, speed * Time.deltaTime);
            }

            if (hasMoved)
            {
                var teleporter = Store.CurrentTeleporters().Find((tele) => tele.sourceTile.X == Store.player.GridX && tele.sourceTile.Y == Store.player.GridY);
                if (teleporter != null)
                {
                    Store.currentRegion = teleporter.destination;
                    EventHub.Raise(EventId.Transition, this, teleporter);
                }
            }

            transform.position = new Vector2(Store.player.X, Store.player.Y);
        }
    }
}
