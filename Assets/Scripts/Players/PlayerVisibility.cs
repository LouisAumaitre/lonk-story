using System;
using System.Collections.Generic;
using LonkStory.Constants;
using LonkStory.Storage;
using LonkStory.Utilities;
using UnityEngine;

namespace LonkStory.Players
{
    public class PlayerVisibility : MonoBehaviour
    {
        private EventHelper _playerChangedTileEvent = new EventHelper(EventId.PlayerChangedTile);

        private List<Tile> _visibleTiles = new List<Tile>();

        void Awake()
        {
            _playerChangedTileEvent.Subscribe(0, (sender, ignored) =>
            {
                _UpdateVisibility();
            });
        }

        void OnDestroy()
        {
            _playerChangedTileEvent.Unsubscribe();
        }

        private static List<Vector2i> Bresenham(int x0, int y0, int x1, int y1)
        {
            List<Vector2i> line = new List<Vector2i>();

            int dx = Mathf.Abs(x1 - x0), sx = x0 < x1 ? 1 : -1;
            int dy = Mathf.Abs(y1 - y0), sy = y0 < y1 ? 1 : -1;
            int err = (dx > dy ? dx : -dy) / 2, e2;
            for (;;)
            {
                line.Add(new Vector2i { X = x0, Y = y0 });
                if (x0 == x1 && y0 == y1) break;
                e2 = err;
                if (e2 > -dx) { err -= dy; x0 += sx; }
                if (e2 < dy) { err += dx; y0 += sy; }
            }

            return line;
        }


        private void _UpdateVisibility()
        {
            foreach (Tile tile in _visibleTiles)
            {
                tile.Visible = false;
            }

            _visibleTiles.Clear();

            int width = 30;
            int height = 15;

            Tile self = Store.CurrentMap().It(Store.player.GridX, Store.player.GridY).Value;

            Utils.Square(Store.player.GridX - width / 2, Store.player.GridY - height / 2, width, height, (x, y) =>
            {
                var lineOfSight = Bresenham(Store.player.GridX, Store.player.GridY, x, y);
                foreach (Vector2i pos in lineOfSight)
                {
                    if (pos == Store.player.GetPosition())
                    {
                        continue;
                    }

                    Tile tile = Store.CurrentMap().It(pos.X, pos.Y).Value;
                    if (tile != null)
                    {
                        tile.Visible = true;
                        _visibleTiles.Add(tile);
                        if (tile.Altitude > self.Altitude || tile.Type == TileType.BuiltGrass || tile.Type == TileType.TreeWall)
                        {
                            break;
                        }
                    }
                }
            });
        }

        void Start()
        {
            _UpdateVisibility();
        }
    }
}