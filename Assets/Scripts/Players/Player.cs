﻿using System.Collections;
using System.Collections.Generic;
using LonkStory.Constants;
using LonkStory.Utilities;
using UnityEngine;

namespace LonkStory.Players
{
    public class Player : Creature
    {
        private int _life = 6;
        public int maxLife { get; set; }

        public int life
        {
            get { return _life; }
            set
            {
                if (value < 0)
                    _life = 0;
                else if (value >= maxLife * 3)
                    _life = maxLife * 3;
                else
                    _life = value;

                Binding.ValueChanged("playerLife", this);
            }
        }

        public Player(int x, int y) : base(x, y)
        {
            maxLife = 3;
            life = maxLife;
        }

        public new bool Move(Direction dir, float speed)
        {
            int lastGridX = _gridX;
            int lastGridY = _gridY;

            bool hasMoved = base.Move(dir, speed);

            if (lastGridX != _gridX || lastGridY != _gridY)
            {
                EventHub.Raise(EventId.PlayerChangedTile, this, null);
            }

            return hasMoved;
        }

        public Vector2i GetPosition()
        {
            return new Vector2i { X = _gridX, Y = _gridY };
        }

        public Vector2 GetPositionFloat()
        {
            return new Vector2(_gridX, _gridY);
        }
    }
}
