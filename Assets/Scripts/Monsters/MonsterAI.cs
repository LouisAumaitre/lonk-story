﻿using System.Collections;
using System.Collections.Generic;
using LonkStory.AI;
using LonkStory.Constants;
using LonkStory.Players;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory.Monsters
{
    public class MonsterAI : MonoBehaviour
    {
        public Monster monster;
        private DecisionTree _ai;

        /*
            Decision tree data.
         */
        private Player player = null;

        void Start()
        {
            _ai = null; //new DecisionTree(this, GetMonsterAIFromType(monster.Type));
        }

        void Update()
        {
            if (_ai != null && !_ai.IsWorking)
            {
                _ai.Next();
            }
            transform.position = new Vector2(monster.X, monster.Y);
        }

        private DecisionNode GetMonsterAIFromType(MonsterType type)
        {
            // FIXME: Handle more monster later, for now, simple SLIME
            if (type != MonsterType.SLIME)
            {
                return null;
            }

            return new DecisionNode()
                .Add(new DecisionNode(() => player != null)
                    .Add(new DecisionNode(
                        () => IsPlayerNear(4),
                        MoveTowardPlayer
                        )
                    )
                    .Add(new DecisionNode(
                        () => true,
                        ForgetPlayer
                        )
                    )
                )
                .Add(new DecisionNode(
                    () => IsPlayerNear(4),
                    SeePlayer
                    )
                )
                .Add(new DecisionNode(
                    () => true,
                    IdleMovement
                    )
                );
        }

        /*
            AI nodes.
         */

        private IEnumerator MoveTowardPlayer()
        {
            var dir = new Vector2(monster.GridX, monster.GridY) - new Vector2(player.GridX, player.GridY);
            dir.Normalize();
            Direction nextDirection = Direction.LEFT;
            if (dir.x > 0)
            {
                nextDirection = Direction.LEFT;
            }
            else if (dir.x < 0)
            {
                nextDirection = Direction.RIGHT;
            }
            else if (dir.y > 0)
            {
                nextDirection = Direction.BOTTOM;
            }
            else
            {
                nextDirection = Direction.TOP;
            }

            monster.Move(nextDirection, 4 * Time.deltaTime);
            yield return null;
        }

        private bool IsPlayerNear(float distance)
        {
            return Vector2.Distance(new Vector2(Store.player.X, Store.player.Y), new Vector2(monster.X, monster.Y)) < distance;
        }

        private IEnumerator SeePlayer()
        {
            player = Store.player;
            yield return null;
        }

        private IEnumerator ForgetPlayer()
        {
            player = null;
            yield return null;
        }

        private IEnumerator IdleMovement()
        {
            var rand = UnityEngine.Random.Range(0, 4);

            monster.Move((Direction)rand, 2 * Time.deltaTime);
            yield return null;
        }
    }
}
