using LonkStory.AI;

namespace LonkStory.Monsters
{
    public class Monster : Creature
    {
        private MonsterType _type;

        public MonsterType Type { get { return _type; } }

        public Monster(int x, int y, MonsterType type) : base(x, y)
        {
            _type = type;
        }
    }
}