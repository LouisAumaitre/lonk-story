using UnityEngine;

namespace LonkStory
{
    public static class ActionManager
    {
        public static ActionState currentState = ActionState.Game;

        public static bool AttackUp { get { return currentState == ActionState.Game && Input.GetKeyDown(KeyCode.UpArrow); } }
        public static bool AttackDown { get { return currentState == ActionState.Game && Input.GetKeyDown(KeyCode.DownArrow); } }
        public static bool AttackLeft { get { return currentState == ActionState.Game && Input.GetKeyDown(KeyCode.LeftArrow); } }
        public static bool AttackRight { get { return currentState == ActionState.Game && Input.GetKeyDown(KeyCode.RightArrow); } }

        public static bool MoveUp { get { return currentState == ActionState.Game && Input.GetKey(KeyCode.W); } }
        public static bool MoveDown { get { return currentState == ActionState.Game && Input.GetKey(KeyCode.S); } }
        public static bool MoveLeft { get { return currentState == ActionState.Game && Input.GetKey(KeyCode.A); } }
        public static bool MoveRight { get { return currentState == ActionState.Game && Input.GetKey(KeyCode.D); } }
    }
}