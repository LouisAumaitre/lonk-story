using System;
using LonkStory.Constants;

namespace LonkStory
{
    public class EventHelper
    {
        private string _key;
        private EventId _id;
        private Action<object, object> _callback;

        public EventHelper(EventId id)
        {
            _id = id;
        }

        public void Subscribe(int priority, Action<object, object> callback)
        {
            _callback = callback;
            _key = EventHub.Subscribe(_id, _callback, priority);
        }

        public void Unsubscribe()
        {
            EventHub.Unsubscribe(_id, _key);
        }
    }
}