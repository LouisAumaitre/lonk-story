using System.Collections;
using System.Collections.Generic;
using LonkStory.Constants;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory
{
    public class Creature
    {
        protected int _gridX;
        protected int _gridY;
        private Vector2 velocity = Vector2.zero;
        private Vector2 tileRatio = Vector2.zero;

        public int GridX { get { return _gridX; } }
        public int GridY { get { return _gridY; } }

        public float X { get { return _gridX + tileRatio.x; } }
        public float Y { get { return _gridY + tileRatio.y; } }

        public Creature(int x, int y)
        {
            _gridX = x;
            _gridY = y;
            tileRatio.x = 0.5f;
            tileRatio.y = 0.5f;
        }

        private bool HasCollision(int dx, int dy)
        {
            if (Input.GetKey(KeyCode.LeftControl)) {
                return false;
            }
            
            var tile = Store.CurrentMap().It(_gridX + dx, _gridY + dy).Value;
            if (tile == null)
                return true;
            return tile.HasCollision;
        }

        public void SetPosition(Utilities.Vector2i pos)
        {
            _gridX = pos.X;
            _gridY = pos.Y;
            tileRatio.x = 0.5f;
            tileRatio.y = 0.5f;
        }

        public bool Move(Direction dir, float speed)
        {
            switch (dir)
            {
                case Direction.TOP:
                    if (HasCollision(0, 1) && tileRatio.y >= 0.7f)
                        return false;
                    tileRatio.y += speed;
                    while (tileRatio.y > 1)
                    {
                        tileRatio.y--;
                        _gridY++;
                    }
                    break;
                case Direction.BOTTOM:
                    if (HasCollision(0, -1) && tileRatio.y <= 0.3f)
                        return false;
                    tileRatio.y -= speed;
                    while (tileRatio.y < 0)
                    {
                        tileRatio.y++;
                        _gridY--;
                    }
                    break;
                case Direction.LEFT:
                    if (HasCollision(-1, 0) && tileRatio.x <= 0.3f)
                        return false;
                    tileRatio.x -= speed;
                    while (tileRatio.x < 0)
                    {
                        tileRatio.x++;
                        _gridX--;
                    }
                    break;
                case Direction.RIGHT:
                    if (HasCollision(1, 0) && tileRatio.x >= 0.7f)
                        return false;
                    tileRatio.x += speed;
                    while (tileRatio.x > 1)
                    {
                        tileRatio.x--;
                        _gridX++;
                    }
                    break;
                default:
                    break;
            }

            return true;
        }
    }
}
