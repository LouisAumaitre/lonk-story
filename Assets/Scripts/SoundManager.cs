using LonkStory.Constants;
using LonkStory.Storage;
using UnityEngine;

namespace LonkStory
{
    public class SoundManager : MonoBehaviour
    {
        public AudioClip[] plainClips;
        public AudioClip[] villageClips;
        public AudioClip[] forestClips;

        public AudioSource musicSource;

        EventHelper _transitionEvent = new EventHelper(EventId.Transition);

        private void _AssignMusic(Region region)
        {
            switch (region)
            {
                case Region.Plain:
                    musicSource.clip = plainClips[Random.Range(0, plainClips.Length)];
                    break;
                case Region.Forest:
                    musicSource.clip = forestClips[Random.Range(0, forestClips.Length)];
                    break;
                case Region.Village:
                    musicSource.clip = villageClips[Random.Range(0, villageClips.Length)];
                    break;
                default:
                    musicSource.clip = forestClips[Random.Range(0, forestClips.Length)];
                    break;
            }
        }

        private void PlayMusic(Region region)
        {
            _AssignMusic(region);
            musicSource.Play();
        }

        void Awake()
        {
            musicSource.loop = true;
            _transitionEvent.Subscribe(100, (sender, args) =>
            {
                Teleporter teleporter = args as Teleporter;
                if (teleporter != null)
                {
                    PlayMusic(teleporter.destination);
                }
            });
        }

        void Start() {
            PlayMusic(Region.Plain);
        }

        void OnDestroy()
        {
            _transitionEvent.Unsubscribe();
        }
    }
}