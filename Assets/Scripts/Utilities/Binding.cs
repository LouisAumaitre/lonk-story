using System;
using System.Collections.Generic;
using UnityEngine;

namespace LonkStory.Utilities
{
    public static class Binding
    {
        private static Dictionary<string, Action<object>> _callbacks = new Dictionary<string, Action<object>>();

        public static void WatchValue(string name, Action<object> onValueChanged)
        {
            if (_callbacks.ContainsKey(name))
            {
                _callbacks[name] += onValueChanged;
            }
            else
            {
                _callbacks.Add(name, onValueChanged);
            }
        }

        public static void UnwatchValue(string name, Action<object> onValueChanged)
        {
            if (_callbacks.ContainsKey(name))
            {
                _callbacks[name] -= onValueChanged;
            }
        }

        public static void ValueChanged(string name, object nextValue)
        {
            if (_callbacks.ContainsKey(name))
            {
                _callbacks[name].Invoke(nextValue);
            }
        }
    }
}