using System;
using UnityEngine;

namespace LonkStory.Utilities
{
    public class SmartArray<T>
    {
        private T[] _array;
        private int _width;
        public int width { get { return _width; } }
        private int _height;
        public int height { get { return _height; } }

        public SmartArray(int width, int height, Func<int, int, T> createInitialValue)
        {
            _width = width;
            _height = height;
            _array = Utils.CreateArray(width, height, createInitialValue);
        }

        public Sentry It(int x, int y)
        {
            return new Sentry(this, x + y * _width);
        }

        public T[] ToArray()
        {
            return _array;
        }

        public bool CheckBounds(int x, int y)
        {
            return CheckBounds(x % _width, y / _width);
        }

        public bool CheckBounds(int index)
        {
            return index >= 0 && index < _array.Length;
        }

        public T this[int x, int y]
        {
            get
            {
                return _array[x + y * _width];
            }

            set
            {
                _array[x + y * _width] = value;
            }
        }

        private T this[int index]
        {
            get
            {
                return _array[index];
            }
        }

        public T[] Filter(Predicate<T> predicate)
        {
            return Array.FindAll(_array, predicate);
        }

        public class Sentry
        {
            private readonly int _index;
            private SmartArray<T> _array;

            private int _Width { get { return _array._width; } }

            public Sentry(SmartArray<T> array, int index)
            {
                if (index < 0 || index >= array.height * array.width)
                    _index = -1;
                else
                    _index = index;
                _array = array;
            }

            public T Value
            {
                get
                {
                    if (_array.CheckBounds(_index))
                    {
                        return _array[_index];
                    }
                    return default(T);
                }
            }

            public Sentry Translate(int dx, int dy)
            {
                int x = _index % _Width;
                int y = _index / _Width;

                int nextX = x + dx;
                int nextY = y + dy;

                if (nextX < 0 || nextX >= _Width || nextY < 0 || nextY >= _array._height)
                    return new Sentry(_array, -1);

                // This formula is correct only for positive coordinates, that's why we check that above.
                int nextIndex = nextX + nextY * _Width;
                if (_array.CheckBounds(nextIndex))
                    return new Sentry(_array, nextIndex);
                else
                    return new Sentry(_array, -1);
            }

            public Sentry Left
            {
                get
                {
                    return Translate(-1, 0);
                }
            }

            public Sentry Right
            {
                get
                {
                    return Translate(1, 0);
                }
            }

            public Sentry Top
            {
                get
                {
                    return Translate(0, 1);
                }
            }

            public Sentry Bottom
            {
                get
                {
                    return Translate(0, -1);
                }
            }
        }
    }
}