using System;
using UnityEngine;

namespace LonkStory.Utilities
{
    public static class Utils
    {
        public static T[] CreateArray<T>(int length, T initialValue)
        {
            T[] array = new T[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = initialValue;
            }
            return array;
        }

        public static T[] CreateArray<T>(int length, Func<int, T> CreateInitialValue)
        {
            T[] array = new T[length];
            for (int i = 0; i < length; i++)
            {
                array[i] = CreateInitialValue(i);
            }
            return array;
        }

        public static T[] CreateArray<T>(int width, int height, Func<int, int, T> CreateInitialValue)
        {
            T[] array = new T[width * height];
            for (int y = 0; y < height; y++)
            {
                for (int x = 0; x < width; x++)
                {
                    array[x + y * width] = CreateInitialValue(x, y);
                }
            }

            return array;
        }

        public static T Get<T>(this T[] array, int x, int y, int width)
        {
            return array[x + y * width];
        }

        public static Vector3 XY(this Vector3 vec, Vector3 other) {
            return new Vector3(other.x, other.y, vec.z);
        }

        public static void SetXY(this Transform transform, Vector3 other) {
            transform.position = transform.position.XY(other);
        }
        
        public static void Square(int offsetX, int offsetY, int width, int height, Action<int, int> action)
        {
            for (int x = offsetX; x < offsetX + width; x++)
            {
                if (x == offsetX || x == offsetX + width - 1)
                {
                    for (int y = offsetY; y < offsetY + height; y++)
                    {
                        if (action != null) action(x, y);
                    }
                }
            }

            for (int y = offsetY; y < offsetY + height; y++)
            {
                if (y == offsetY || y == offsetY + height - 1)
                {
                    for (int x = offsetX + 1; x < offsetX + width - 1; x++)
                    {
                        if (action != null) action(x, y);
                    }
                }
            }
        }
    }
}