﻿using UnityEngine;
using UnityEditor;

public class Room {
    public int id { get; set; }
    public int group { get; set; }
    public int X { get; set; }
    public int Y { get; set; }

    public bool north { get; set; }
    public bool south { get; set; }
    public bool east { get; set; }
    public bool west { get; set; }
    
    public int premadeIndex { get; set; }

    public Room(int _id, int x, int y, bool n, bool s, bool w, bool e)
    {
        id = _id;
        group = id;
        X = x;
        Y = y;

        north = n;
        south = s;
        west = w;
        east = e;

        premadeIndex = -1;
    }

}