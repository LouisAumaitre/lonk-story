namespace LonkStory.Utilities
{
    public class Vector2i {
        public int X { get; set; }
        public int Y { get; set; }

        public Vector2i(int x, int y)
        {
            X = x;
            Y = y;
        }

        public Vector2i() { }
    }
}