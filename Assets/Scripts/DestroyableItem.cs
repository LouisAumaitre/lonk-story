﻿using UnityEngine;
using UnityEditor;
using LonkStory.Storage;
using LonkStory;

namespace LonkStory
{
    public class DestroyableItem : MonoBehaviour
    {
        public Tile BoundTile { get; set; }

        public void GetDestroyed()
        {
            if (UnityEngine.Random.Range(0, 4) == 0)
            {
                GameObjectManager.singleton.InstantiateHeart(transform.position);
            }
            Destroy(gameObject);
            BoundTile.HasCollision = false;
        }
    }
}