using LonkStory.Utilities;
using UnityEngine;

namespace LonkStory.Storage
{
    public enum TileType
    {
        None,
        Grass,
        BuiltGrass,
        Sea,
        CliffBottom,
        CliffTop,
        Bush,
        House, // creates a prefab
        Tree, // creates a prefab
        TreeWall,
        Slab,
        WallTop,
        WallBottom,
        Firepost,
        AncientSlab,
        Rock
    }

    public class Tile // FIXME
    {
        public int X { get; set; }
        public int Y { get; set; }
        public float Altitude { get; set; }
        public TileType Type { get; set; }
        public bool HasCollision { get; set; }
        public bool Visible
        {
            get { return _visible; }
            set { _visible = value; Binding.ValueChanged("tileVisibility" + X + " " + Y, this); }
        }

        public bool Parcoured { get; set; } // used for generation

        private bool _visible = false;

        public static Tile Zero(TileType type = TileType.None, bool collision = false)
        {
            return new Tile
            {
                X = 0,
                Y = 0,
                Altitude = 0,
                Type = type,
                HasCollision = collision,
                Parcoured = false
            };
        }

        public Vector2 ToVector2() {
            return new Vector2(X, Y);
        }

        public override string ToString()
        {
            return string.Format("X:{0}, Y:{1}, Alt:{2} Type:{3}", X, Y, Altitude, Type);
        }
    }
}