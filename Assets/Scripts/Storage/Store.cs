using LonkStory.Utilities;
using LonkStory.Players;
using System.Collections.Generic;
using LonkStory.Monsters;
using LonkStory.Constants;

namespace LonkStory.Storage
{

    // Keep all game persistant data here.
    public static class Store
    {
        public static Player player;
        public static Region currentRegion = Region.Plain;
        private static Dictionary<Region, Map> _maps = new Dictionary<Region, Map>();

        public static SmartArray<Tile> CurrentMap()
        {
            return _maps[currentRegion].Tiles;
        }
        public static List<Monster> CurrentMonsters()
        {
            return _maps[currentRegion].Monsters;
        }
        public static List<Teleporter> CurrentTeleporters()
        {
            return _maps[currentRegion].Teleporters;
        }

        public static Map Get(Region region)
        {
            return _maps[region];
        }

        private static void _InitializeMap(Region region) {
            _maps.Add(region, new Map());
            _maps[region].Monsters = new List<Monster>();
            _maps[region].Teleporters = new List<Teleporter>();
        }

        public static void SetTiles(Region region, SmartArray<Tile> tiles)
        {
            if (!_maps.ContainsKey(region))
                _InitializeMap(region);

            _maps[region].Tiles = tiles;
        }

        public static void SetMonsters(Region region, List<Monster> monsters)
        {
            if (!_maps.ContainsKey(region))
                _InitializeMap(region);

            _maps[region].Monsters = monsters;
        }

        public static void SetTeleporters(Region region, List<Teleporter> teleporters)
        {
            if (!_maps.ContainsKey(region))
                _InitializeMap(region);

            _maps[region].Teleporters = teleporters;
        }

        public static void AddTeleporter(Region region, Teleporter teleporter)
        {
            if (!_maps.ContainsKey(region))
                return;

            if (_maps[region].Teleporters == null)
                _maps[region].Teleporters = new List<Teleporter>();
            
            _maps[region].Teleporters.Add(teleporter);
        }
    }
}