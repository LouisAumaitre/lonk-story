using LonkStory.Constants;
using LonkStory.Utilities;

namespace LonkStory.Storage {
    public class Teleporter {
        public Vector2i sourceTile;
        public Vector2i destinationTile;
        public Region destination;
    }
}