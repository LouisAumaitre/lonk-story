using System.Collections.Generic;
using LonkStory.Monsters;
using LonkStory.Utilities;
using LonkStory.Constants;

namespace LonkStory.Storage
{
    public class Map
    {
        public SmartArray<Tile> Tiles { get; set; }
        public List<Monster> Monsters { get; set; }
        public List<Teleporter> Teleporters { get; set; }
        public Dictionary<Region, Vector2i> Entrances { get; set; }
    }
}