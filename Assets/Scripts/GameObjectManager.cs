using System;
using System.Collections.Generic;
using LonkStory.Utilities;
using UnityEngine;
using LonkStory.Players;
using LonkStory.Monsters;
using LonkStory.Storage;
using LonkStory.Constants;
using System.Linq;

namespace LonkStory
{
    using uRandom = UnityEngine.Random;

    // This manager is in charge of instantiating, keep track of, and destroying GameObjects.
    // It should be the only one able to operate on GameObject life duration.
    // (yet other scripts can modify the GameObject or one of his Component)
    public class GameObjectManager : MonoBehaviour
    {
        /*
            Singleton
         */

        // We make sure it is accessible from anywhere in the program.
        private static GameObjectManager _singleton = null;

        public static GameObjectManager singleton { get { return _singleton; } }

        void Awake()
        {
            if (_singleton == null)
            {
                _singleton = this;
                _LateAwake();
            }
            else
            {
                Destroy(this.gameObject);
            }
        }

        /*
            GameObjectManager
        */

        public Sprite[] grassSprites;
        public Sprite[] bushSprites;
        public Sprite[] flowerSprites;
        public Sprite[] seaSprites;
        public Sprite[] rockSprites;
        public Sprite[] treeSprites;
        public Sprite[] slabSprites;
        public Sprite[] ancientSlabSprites;
        public Sprite[] firepostSprites;
        /*
         * WALL SPRITES
         * 
         */
        public Sprite[] wallBottomSprites;
        public Sprite[] wallTopSprites;
        public Sprite cliffSingle;
        public Sprite dark;

        /*
            Prefabs and parents
         */

        public PickableItem heartPrefab;
        public GameObject tilePrefab;
        public GameObject topDecorationPrefab;
        public GameObject bottomDecorationPrefab;
        public GameObject blueHouseTopPrefab;
        public GameObject blueHouseBottomPrefab;
        public GameObject treeTopPrefab;
        public GameObject treeBottomPrefab;
        public Transform mapParent;

        public GameObject playerPrefab;

        public GameObject monsterPrefab;

        public GameObject GetMonsterPrefabFromType(MonsterType type)
        {
            return monsterPrefab;
        }

        private Dictionary<Tile, GameObject> _tilesToGameObjects = new Dictionary<Tile, GameObject>();
        private GameObject _playerGameObject;
        private Dictionary<Monster, GameObject> _monsterToGameObjects = new Dictionary<Monster, GameObject>();

        private EventHelper _transitionEvent = new EventHelper(EventId.Transition);

        private void _LateAwake()
        {
            _transitionEvent.Subscribe(0, (sender, arg) =>
            {
                Teleporter tele = arg as Teleporter;
                if (tele == null)
                    return;
                Instantiate(Store.Get(tele.destination));
                Instantiate(Store.player);
            });
        }

        void OnDestroy()
        {
            _transitionEvent.Unsubscribe();
        }

        private void AssignSprite(SmartArray<Tile> map, Tile tile, GameObject tileGameObject)
        {
            var IsTreeWall = new Func<SmartArray<Tile>.Sentry, bool>(i =>
            {
                return i.Value == null || i.Value.Type == TileType.TreeWall;
            });
            var Altitude = new Func<SmartArray<Tile>.Sentry, float>(i =>
            {
                if (i.Value == null)
                    return 0;
                else
                    return i.Value.Altitude;
            });

            var it = map.It(tile.X, tile.Y);
            switch (tile.Type)
            {
                case TileType.Grass:
                case TileType.BuiltGrass:
                case TileType.House:
                case TileType.Tree:
                case TileType.TreeWall:
                    if (tile.Type == TileType.Grass && UnityEngine.Random.value < 0.05f)
                    {
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = flowerSprites[UnityEngine.Random.Range(0, flowerSprites.Length)];
                    }
                    else
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = grassSprites[UnityEngine.Random.Range(0, grassSprites.Length)];

                    if (tile.Type == TileType.House)
                    {
                        GameObject houseTopGameObject = Instantiate(blueHouseTopPrefab, new Vector3(tile.X, tile.Y + 2, 0), Quaternion.identity);
                        houseTopGameObject.transform.SetParent(mapParent);
                        GameObject houseBottomGameObject = Instantiate(blueHouseBottomPrefab, new Vector3(tile.X, tile.Y, 0), Quaternion.identity);
                        houseBottomGameObject.transform.SetParent(mapParent);
                    }
                    if (tile.Type == TileType.Tree)
                    {
                        GameObject treeTopGameObject = Instantiate(treeTopPrefab, new Vector3(tile.X, tile.Y, 0), Quaternion.identity);
                        treeTopGameObject.transform.SetParent(mapParent);
                        GameObject treeBottomGameObject = Instantiate(treeBottomPrefab, new Vector3(tile.X, tile.Y, 0), Quaternion.identity);
                        treeBottomGameObject.transform.SetParent(mapParent);
                    }
                    if (tile.Type == TileType.TreeWall)
                    {
                        int treeX = (tile.X + 2) % 3;
                        int treeY = (tile.Y + 1) % 3;
                        if (treeY != 2 || IsTreeWall(it.Bottom))
                        {
                            GameObject topTreeWallGameObject = Instantiate(topDecorationPrefab, new Vector3(tile.X + 0.5f, tile.Y + 0.5f, 0), Quaternion.identity);
                            topTreeWallGameObject.transform.SetParent(tileGameObject.transform);
                            topTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[treeX + 8 - 4 * treeY];
                            if (treeX == 0 && IsTreeWall(it.Left) && IsTreeWall(it.Left.Bottom))
                            {
                                if (IsTreeWall(it.Right) && IsTreeWall(it.Right.Bottom))
                                    topTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[22 - treeY];
                                else
                                    topTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[11 - 4 * treeY];
                            }
                        }
                        if (treeY == 2 && IsTreeWall(it.Top))
                        {
                            GameObject bottomTreeWallGameObject = Instantiate(bottomDecorationPrefab, new Vector3(tile.X + 0.5f, tile.Y + 0.5f, 0), Quaternion.identity);
                            bottomTreeWallGameObject.transform.SetParent(tileGameObject.transform);
                            bottomTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[treeX + 12];
                            if (treeX == 0 && IsTreeWall(it.Left))
                            {
                                if (IsTreeWall(it.Right))
                                    bottomTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[23];
                                else
                                    bottomTreeWallGameObject.GetComponent<SpriteRenderer>().sprite = treeSprites[15];
                            }
                        }
                    }
                    break;
                case TileType.Sea:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = seaSprites[UnityEngine.Random.Range(0, seaSprites.Length)];
                    break;
                case TileType.Bush:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = bushSprites[0];
                    GameObject bushGameObject = Instantiate(bottomDecorationPrefab, new Vector3(tile.X + 0.5f, tile.Y + 0.5f, 0), Quaternion.identity);
                    bushGameObject.transform.SetParent(tileGameObject.transform);
                    bushGameObject.GetComponent<SpriteRenderer>().sprite = bushSprites[UnityEngine.Random.Range(1, bushSprites.Length)];
                    bushGameObject.AddComponent<BoxCollider2D>();
                    bushGameObject.GetComponent<BoxCollider2D>().isTrigger = true;
                    bushGameObject.AddComponent<DestroyableItem>();
                    bushGameObject.GetComponent<DestroyableItem>().BoundTile = tile;
                    break;
                case TileType.Rock:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = rockSprites[UnityEngine.Random.Range(0, rockSprites.Length)];
                    break;
                case TileType.Slab:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = slabSprites[UnityEngine.Random.Range(0, slabSprites.Length)];
                    break;
                case TileType.AncientSlab:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = ancientSlabSprites[UnityEngine.Random.Range(0, ancientSlabSprites.Length)];
                    break;
                case TileType.Firepost:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = firepostSprites[UnityEngine.Random.Range(0, firepostSprites.Length)];
                    break;
                case TileType.CliffTop:
                    AssignTopWallSprite(map, tile, tileGameObject, TileType.CliffBottom, TileType.CliffTop, wallTopSprites, rockSprites[UnityEngine.Random.Range(0, rockSprites.Length)]);
                    break;
                case TileType.CliffBottom:
                    AssignBottomWallSprite(map, tile, tileGameObject, TileType.CliffBottom, TileType.CliffTop, wallBottomSprites, rockSprites[UnityEngine.Random.Range(0, rockSprites.Length)]);
                    break;
                case TileType.WallTop:
                    AssignTopWallSprite(map, tile, tileGameObject, TileType.WallBottom, TileType.WallTop, wallTopSprites, dark);
                    break;
                case TileType.WallBottom:
                    AssignBottomWallSprite(map, tile, tileGameObject, TileType.WallBottom, TileType.WallTop, wallBottomSprites, firepostSprites[0]);
                    break;
                default:
                    break;
            }
        }

        private void AssignBottomWallSprite(SmartArray<Tile> map, Tile tile, GameObject tileGameObject, TileType bottom, TileType top, Sprite[] sprites, Sprite defaultSprite)
        {
            var WallValue = new Func<SmartArray<Tile>.Sentry, int>(i =>
            {
                if (i.Value == null || i.Value.Type == bottom)
                    return 1;
                else if (i.Value.Type == top)
                    return 2;
                else
                    return 0;
            });
            var IsWall = new Func<SmartArray<Tile>.Sentry, bool>(i =>
            {
                return i.Value == null || i.Value.Type == bottom || i.Value.Type == top;
            });

            var it = map.It(tile.X, tile.Y);

            int choice = ((WallValue(it.Top) * 3 + WallValue(it.Left)) * 3 + WallValue(it.Right)) * 3 + WallValue(it.Bottom);
            switch (choice)
            {
                case 4: // north-west ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[0];
                    break;
                case 10: // north-east ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[3];
                    break;
                case 14:
                    if (IsWall(it.Left.Left) && !IsWall(it.Right.Right)) // top-north-west ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[2];
                    else if (!IsWall(it.Left.Left) && IsWall(it.Right.Right)) // top-north-east ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];
                    else // north
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[16 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 34:
                    if (IsWall(it.Top.Top) && !IsWall(it.Bottom.Bottom)) // left-south-west ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[10];
                    else if (!IsWall(it.Top.Top) && IsWall(it.Bottom.Bottom)) // left-north-west ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[11];
                    else // west
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[19 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 36: // south-east ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[6];
                    break;
                case 30: // south-west ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[9];
                    break;
                case 44: // north-west int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[12];
                    break;
                case 46:
                    if (IsWall(it.Top.Top) && !IsWall(it.Bottom.Bottom)) // right-south-east ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[5];
                    else if (!IsWall(it.Top.Top) && IsWall(it.Bottom.Bottom)) // right-north-east ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[4];
                    else // right
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[17 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 50: // north-east int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[13];
                    break;
                case 66:
                    if (IsWall(it.Left.Left) && !IsWall(it.Right.Right)) // bottom-south-west ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[7];
                    else if (!IsWall(it.Left.Left) && IsWall(it.Right.Right)) // bottom-south-east ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[8];
                    else // south
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[18 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 76: // south-east int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[14];
                    break;
                case 70: // south-west int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[15];
                    break;
                default:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = defaultSprite;
                    break;
            }
        }

        private void AssignTopWallSprite(SmartArray<Tile> map, Tile tile, GameObject tileGameObject, TileType bottom, TileType top, Sprite[] sprites, Sprite defaultSprite)
        {
            var WallValue = new Func<SmartArray<Tile>.Sentry, int>(i =>
            {
                if (i.Value == null || i.Value.Type == top)
                    return 1;
                else if (i.Value.Type == bottom)
                    return 2;
                else
                    return 0;
            });
            var IsWall = new Func<SmartArray<Tile>.Sentry, bool>(i =>
            {
                return i.Value == null || i.Value.Type == bottom || i.Value.Type == top;
            });

            var it = map.It(tile.X, tile.Y);

            int choice = ((WallValue(it.Top) * 3 + WallValue(it.Left)) * 3 + WallValue(it.Right)) * 3 + WallValue(it.Bottom);
            switch (choice)
            {
                case 4: // south-east int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[4];
                    break;
                case 10: // south-west int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[7];
                    break;
                case 14:
                    if (IsWall(it.Left.Left) && !IsWall(it.Right.Right)) // top-south-west int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[6];
                    else if (!IsWall(it.Left.Left) && IsWall(it.Right.Right)) // top-south-east int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[5];
                    else // south
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[18 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 34:
                    if (IsWall(it.Top.Top) && !IsWall(it.Bottom.Bottom)) // left-south-east int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[14];
                    else if (!IsWall(it.Top.Top) && IsWall(it.Bottom.Bottom)) // left-north-east int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[15];
                    else // east
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[17 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 36: // north-west int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[10];
                    break;
                case 30: // north-east int.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[13];
                    break;
                case 44: // south-east ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[2];
                    break;
                case 46:
                    if (IsWall(it.Top.Top) && !IsWall(it.Bottom.Bottom)) // right-north-west int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[9];
                    else if (!IsWall(it.Top.Top) && IsWall(it.Bottom.Bottom)) // right-south-west int.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[8];
                    else // west
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[19 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 50: // south-west ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[3];
                    break;
                case 66:
                    if (IsWall(it.Left.Left) && !IsWall(it.Right.Right)) // bottom-north-west ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[11];
                    else if (!IsWall(it.Left.Left) && IsWall(it.Right.Right)) // bottom-north-east ext.
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[12];
                    else // north
                        tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[16 + 4 * uRandom.Range(0, (sprites.Length - 16) / 4)];
                    break;
                case 70: // north-east ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[1];
                    break;
                case 76: // north-west ext.
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = sprites[0];
                    break;
                default:
                    tileGameObject.GetComponent<SpriteRenderer>().sprite = defaultSprite;
                    break;
            }
        }

        private void AssignCollision(SmartArray<Tile> map, Tile tile, GameObject tileGameObject)
        {
            switch (tile.Type)
            {
                case TileType.Sea:
                case TileType.CliffTop:
                case TileType.CliffBottom:
                case TileType.Rock:
                case TileType.TreeWall:
                case TileType.BuiltGrass:
                case TileType.Bush:
                case TileType.WallTop:
                case TileType.WallBottom:
                case TileType.Firepost:
                    tile.HasCollision = true;
                    break;
                case TileType.House:
                    /*var houseTile = map.It(tile.X, tile.Y).Top;
                    for (int i = 0; i < 5; i++)
                    {
                        var houseTileBis = houseTile;
                        for (int j = 0; j < 4; j++)
                        {
                            if (houseTileBis.Value != null)
                                houseTileBis.Value.HasCollision = true;
                            houseTileBis = houseTileBis.Top;
                        }
                        houseTile = houseTile.Right;
                    }*/

                    break;
                case TileType.Tree:
                    tile.HasCollision = true;
                    /*var treeTile = map.It(tile.X, tile.Y);
                    for (int i = 0; i < 4; i++)
                    {
                        var treeTileBis = treeTile;
                        for (int j = 0; j < 4; j++)
                        {
                            if (treeTileBis.Value != null)
                                treeTileBis.Value.HasCollision = true;
                            treeTileBis = treeTileBis.Top;
                        }
                        treeTile = treeTile.Right;
                    }*/

                    break;
                default:
                    break;
            }
        }

        private void ChangeTileVisibility(Tile tile)
        {
            GameObject tileGameObject = _tilesToGameObjects[tile];

            Transform fog = tileGameObject.transform.Find("Fog");

            if (tile.Visible)
            {
                fog.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, 0);
            }
            else
            {
                fog.GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, .5f);
            }
        }

        public void ChangeTileVisibility(object obj)
        {
            Tile tile = obj as Tile;

            if (!_tilesToGameObjects.ContainsKey(tile))
                return;
            ChangeTileVisibility(tile);
        }

        public void Instantiate(Map map)
        {
            foreach (Tile tile in _tilesToGameObjects.Keys)
            {
                Binding.UnwatchValue("tileVisibility" + tile.X + " " + tile.Y, ChangeTileVisibility);
            }

            _tilesToGameObjects.Clear();
            _monsterToGameObjects.Clear();

            foreach (Transform child in mapParent)
            {
                Destroy(child.gameObject);
            }

            Instantiate(map.Tiles);
            Instantiate(map.Monsters);
            Instantiate(map.Teleporters);
        }

        public void Instantiate(SmartArray<Tile> map)
        {
            Array.ForEach(map.ToArray(), tile =>
            {
                GameObject tileGameObject = Instantiate(tilePrefab, new Vector3(tile.X + 0.5f, tile.Y + 0.5f, 0), Quaternion.identity);
                tileGameObject.name = "tile@" + tile.X + "," + tile.Y;
                tileGameObject.transform.SetParent(mapParent);
                AssignSprite(map, tile, tileGameObject);
                AssignCollision(map, tile, tileGameObject);
                _tilesToGameObjects.Add(tile, tileGameObject);

                ChangeTileVisibility(tile);
                Binding.WatchValue("tileVisibility" + tile.X + " " + tile.Y, ChangeTileVisibility);
            });
        }

        public void Instantiate(List<Monster> monsters)
        {
            monsters.ForEach(m =>
            {
                GameObject monsterGameObject = Instantiate(GetMonsterPrefabFromType(m.Type), new Vector3(m.X, m.Y, 0), Quaternion.identity);
                monsterGameObject.transform.SetParent(mapParent);
                monsterGameObject.GetComponent<MonsterAI>().monster = m;
            });
        }

        public void Instantiate(List<Teleporter> teleporters)
        {
            foreach (Teleporter tele in teleporters)
            {
                int x = tele.sourceTile.X;
                int y = tele.sourceTile.Y;
                foreach (Tile tile in _tilesToGameObjects.Keys)
                {
                    if (tile.X == x && tile.Y == y)
                    {
                        _tilesToGameObjects[tile].GetComponent<SpriteRenderer>().color = Color.red;
                        break;
                    }
                }
            }
        }

        public void Instantiate(Player player)
        {
            if (_playerGameObject != null)
                Destroy(_playerGameObject);
            GameObject playerGameObject = Instantiate(playerPrefab, new Vector3(player.X, player.Y, 0), Quaternion.identity);
            _playerGameObject = playerGameObject;
            FindObjectOfType<CameraFollower>().target = playerGameObject.transform;
        }

        public void Instantiate(PickableItem prefab)
        {
            GameObject item = Instantiate(prefab.gameObject, mapParent) as GameObject;
            Destroy(item, UnityEngine.Random.Range(8, 16));
            item.name = prefab.id.ToString();
            var locations = TerrainGenerator.SpawnLocations(Store.CurrentMap(), TileType.Grass, 1);
            item.transform.localPosition = locations[UnityEngine.Random.Range(0, locations.Count)].ToVector2();
        }

        public void InstantiateHeart(Vector2 position)
        {
            GameObject item = Instantiate(heartPrefab.gameObject, mapParent) as GameObject;
            Destroy(item, UnityEngine.Random.Range(8, 16));
            item.name = heartPrefab.id.ToString();
            item.transform.position = position;
        }
    }
}