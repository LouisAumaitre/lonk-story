using System;
using System.Collections.Generic;
using LonkStory.Constants;

namespace LonkStory
{

    using EventCallback = Action<object, object>;

    public static class EventHub
    {
        private class PriorityChannel
        {
            public List<string> callbackKeys = new List<string>();
            public int priority;
        }

        private static Dictionary<string, EventCallback> _callbackMap = new Dictionary<string, EventCallback>();
        private static Dictionary<EventId, List<PriorityChannel>> _hubs = new Dictionary<EventId, List<PriorityChannel>>();

        public static void Raise(EventId id, object sender, object args)
        {
            if (_hubs.ContainsKey(id))
            {
                _hubs[id].ForEach(chan => {
                    chan.callbackKeys.ForEach(key => {
                        _callbackMap[key].Invoke(sender, args);
                    });
                });
            }
        }

        public static string Subscribe(EventId id, EventCallback callback, int priority = 0)
        {
            string key = Guid.NewGuid().ToString();

            _callbackMap.Add(key, callback);

            if (!_hubs.ContainsKey(id))
            {
                _hubs[id] = new List<PriorityChannel>();
            }

            PriorityChannel channel = _hubs[id].Find(chan => chan.priority == priority);

            if (channel == null)
            {
                channel = new PriorityChannel { priority = priority };
                _hubs[id].Add(channel);
            }

            channel.callbackKeys.Add(key);

            _hubs[id].Sort((a, b) => a.priority - b.priority);

            return key;
        }

        public static void Unsubscribe(EventId id, string subscribeKey)
        {
            if (!_hubs.ContainsKey(id))
            {
                return;
            }

            // Because it would be a hasle to check in which priority channel the callback is, remove it from any (must be one).
            _hubs[id].ForEach(chan =>
            {
                chan.callbackKeys.Remove(subscribeKey);
            });
        }
    }
}