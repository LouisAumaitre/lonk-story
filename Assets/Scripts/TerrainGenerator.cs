﻿using LonkStory.Utilities;
using LonkStory.Players;
using System;
using UnityEngine;
using System.Collections.Generic;
using LonkStory.Storage;
using LonkStory.Constants;
using LonkStory.Monsters;

namespace LonkStory
{
    using uRandom = UnityEngine.Random;

    public class TerrainGenerator : MonoBehaviour
    {
        public SmartArray<Tile> Terrain { get { return Store.CurrentMap(); } }
        public Sprite[] ForestRoomPremades;
        public Sprite[] PlainRoomPremades;
        public Sprite[] VillageRoomPremades;

        public int[] width, height;
        private Vector2i playerStartingPosition;

        // for each region, a list of its teleporters
        private Dictionary<Region, List<Teleporter>> Teleporters;
        // for each region A, a list of regions (B)
        // for each region B, the destination tile in B coming from A
        private Dictionary<Region, Dictionary<Region, Vector2i>> Entrances;

        // for each region, contains a dictionary of colors->tiletypes
        // used to parse premade rooms sprites
        private Dictionary<Region, Dictionary<int, TileType>> EditorColors;

        // get an id from a Color
        private int ColorToInt(Color c)
        {
            int detail = 255; // detail level
            int r = (int)(c.r * detail) * detail * detail;
            int g = (int)(c.g * detail) * detail;
            int b = (int)(c.b * detail);
            return r + g + b;
        }
        private void _SetupEditorColors()
        {
            EditorColors = new Dictionary<Region, Dictionary<int, TileType>>();
            var plainColors = new Dictionary<int, TileType>();
            plainColors.Add(ColorToInt(new Color(1, 1, 1)), TileType.Grass);
            plainColors.Add(ColorToInt(new Color(0, 0, 0)), TileType.CliffTop);
            plainColors.Add(ColorToInt(new Color(1, 0, 0)), TileType.Rock);
            plainColors.Add(ColorToInt(new Color(0, 1, 0)), TileType.Tree);
            plainColors.Add(ColorToInt(new Color(0, 0, 1)), TileType.House);
            plainColors.Add(ColorToInt(new Color(0, 1, 1)), TileType.Bush);
            plainColors.Add(ColorToInt(new Color(1, 0, 1)), TileType.Grass); // previously: player start Position; now: could be reassigned
            EditorColors.Add(Region.Plain, plainColors);
            EditorColors.Add(Region.Village, plainColors);

            var forestColors = new Dictionary<int, TileType>();
            forestColors.Add(ColorToInt(new Color(1, 1, 1)), TileType.Grass);
            forestColors.Add(ColorToInt(new Color(0, 0, 0)), TileType.TreeWall);
            forestColors.Add(ColorToInt(new Color(1, 0, 0)), TileType.Rock);
            forestColors.Add(ColorToInt(new Color(0, 1, 1)), TileType.Bush);
            EditorColors.Add(Region.Forest, forestColors);

            var dungeonColors = new Dictionary<int, TileType>();
            dungeonColors.Add(ColorToInt(new Color(1, 1, 1)), TileType.Slab);
            dungeonColors.Add(ColorToInt(new Color(0, 0, 0)), TileType.WallTop);
            dungeonColors.Add(ColorToInt(new Color(1, 0, 0)), TileType.Firepost);
            dungeonColors.Add(ColorToInt(new Color(0, 1, 1)), TileType.AncientSlab);
            EditorColors.Add(Region.Dungeon, dungeonColors);
        }

        public int Size(Region region)
        {
            return width[(int)region] * height[(int)region];
        }

        void Awake()
        {
            _SetupEditorColors();
            playerStartingPosition = new Vector2i(width[(int)Region.Plain] / 2, height[(int)Region.Plain] / 2);

            Teleporters = new Dictionary<Region, List<Teleporter>>();
            Entrances = new Dictionary<Region, Dictionary<Region, Vector2i>>();
            for (int i = 0; i < (int)Region.NumberOfRegions; i++)
            {
                Teleporters.Add((Region)i, new List<Teleporter>());
                Entrances.Add((Region)i, new Dictionary<Region, Vector2i>());
            }

            // Plain.
            Store.SetTiles(Region.Plain, _GeneratePlain());
            Store.SetMonsters(Region.Plain, SpawnRandomMonsters(Store.Get(Region.Plain), MonsterType.SLIME, (int)(Size(Region.Plain) * 0.0025), TileType.Grass, 1));

            // Village.
            Store.SetTiles(Region.Village, _GenerateVillage());

            // Forest.
            Store.SetTiles(Region.Forest, _GenerateForest());

            // Dungeon.
            Store.SetTiles(Region.Dungeon, _GenerateDungeon());

            // teleporters
            for (int i = 0; i < (int)Region.NumberOfRegions; i++)
            {
                foreach (var tp in Teleporters[(Region)i])
                {
                    if (Entrances[(Region)i].ContainsKey(tp.destination))
                    {
                        tp.destinationTile = Entrances[(Region)i][tp.destination];
                        Store.AddTeleporter((Region)i, tp);
                    }
                }
            }

            Store.player = new Player((int)playerStartingPosition.X, (int)playerStartingPosition.Y);
        }

        void Start()
        {

            GameObjectManager.singleton.Instantiate(Store.Get(Region.Plain));
            GameObjectManager.singleton.Instantiate(Store.player);
        }

        private SmartArray<Tile> _GeneratePlain()
        {
            int region = (int)Region.Plain;
            int roomWidth = 12;
            int roomHeight = 12;
            int mazeWidth = (width[region]) / roomWidth;
            int mazeHeight = (height[region]) / roomHeight;

            SmartArray<Tile> terrain = new SmartArray<Tile>(mazeWidth * roomWidth, mazeHeight * roomHeight,
                (i, j) => new Tile { X = i, Y = j, Altitude = 1, Type = TileType.BuiltGrass });

            SmartArray<Room> rooms = new SmartArray<Room>(mazeWidth, mazeHeight,
                (i, j) => new Room(i + j * mazeWidth, i, j,
                (i > 0 && i < mazeWidth - 1) || j == mazeHeight - 1,
                (i > 0 && i < mazeWidth - 1) || j == 0,
                (j > 0 && j < mazeHeight - 1) || i == 0,
                (j > 0 && j < mazeHeight - 1) || i == mazeWidth - 1));

            Vector2i house = new Vector2i(1, uRandom.Range(1, mazeHeight - 1));
            rooms.It(house.X, house.Y).Value.premadeIndex = 16;
            Vector2i toVillage = new Vector2i(mazeWidth - 1, uRandom.Range(1, mazeHeight));
            rooms.It(toVillage.X, toVillage.Y).Value.east = false;
            rooms.It(toVillage.X, toVillage.Y).Value.south = true;
            rooms.It(toVillage.X, toVillage.Y).Bottom.Value.east = false;
            rooms.It(toVillage.X, toVillage.Y).Bottom.Value.north = true;

            ReadRoomPremades(rooms, terrain, PlainRoomPremades, Region.Plain, roomWidth, roomHeight);

            CreepAltitude(terrain, 0, 0, 0);
            CompleteCliffs(terrain, 0);

            // find the house and set the player
            for (int i = house.X * roomWidth; i < (house.X+1) * roomWidth; i++)
            {
                for (int j = house.Y * roomHeight; j < (house.Y + 1) * roomHeight; j++)
                {
                    if (terrain.It(i, j).Value.Type == TileType.House)
                    {
                        playerStartingPosition = new Vector2i(i + 2, j - 2);
                    }
                }
            }

            // should be replaced with a generic function
            for (int j = 0; j < terrain.height; j++)
            {
                int i = terrain.width - 1;
                if (terrain.It(i, j).Value.Altitude == 1)
                {
                    Teleporters[Region.Plain].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Village });
                }
            }
            Entrances[Region.Village].Add(Region.Plain, new Vector2i(terrain.width - 2, toVillage.Y * roomHeight));
            //var houseLocation = SetLonksHouse(terrain, TileType.None, 1);

            return terrain;
        }

        private SmartArray<Tile> _GenerateVillage()
        {
            int region = (int)Region.Village;
            int roomWidth = 12;
            int roomHeight = 12;
            int mazeWidth = (width[region]) / roomWidth;
            int mazeHeight = (height[region]) / roomHeight;

            SmartArray<Tile> terrain = new SmartArray<Tile>(mazeWidth * roomWidth, mazeHeight * roomHeight,
                (i, j) => new Tile { X = i, Y = j, Altitude = 1, Type = TileType.BuiltGrass });

            SmartArray<Room> rooms = new SmartArray<Room>(mazeWidth, mazeHeight,
                (i, j) => new Room(i + j * mazeWidth, i, j,
                (i > 0 && i < mazeWidth - 1) || j == mazeHeight - 1,
                (i > 0 && i < mazeWidth - 1) || j == 0,
                (j > 0 && j < mazeHeight - 1) || i == 0,
                (j > 0 && j < mazeHeight - 1) || i == mazeWidth - 1));

            for (int i = 1; i < mazeWidth - 1; i++)
            {
                for (int j = 1; j < mazeHeight - 1; j++)
                {
                    rooms.It(i, j).Value.premadeIndex = 16;
                }
            }
            Vector2i toPlain = new Vector2i(0, uRandom.Range(1, mazeHeight));
            rooms.It(toPlain.X, toPlain.Y).Value.west = false;
            rooms.It(toPlain.X, toPlain.Y).Value.south = true;
            rooms.It(toPlain.X, toPlain.Y).Bottom.Value.west = false;
            rooms.It(toPlain.X, toPlain.Y).Bottom.Value.north = true;

            Vector2i toForest = new Vector2i(mazeWidth - 1, uRandom.Range(1, mazeHeight));
            rooms.It(toForest.X, toForest.Y).Value.east = false;
            rooms.It(toForest.X, toForest.Y).Value.south = true;
            rooms.It(toForest.X, toForest.Y).Bottom.Value.east = false;
            rooms.It(toForest.X, toForest.Y).Bottom.Value.north = true;

            ReadRoomPremades(rooms, terrain, PlainRoomPremades, Region.Village, roomWidth, roomHeight);

            CreepAltitude(terrain, 0, 0, 0);
            CreepAltitude(terrain, 0, 0, terrain.height - 1);
            CompleteCliffs(terrain, 0);

            // should be replaced with a generic function
            for (int j = 0; j < terrain.height; j++)
            {
                int i = 0;
                if (terrain.It(i, j).Value.Altitude == 1)
                {
                    Teleporters[Region.Village].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Plain });
                }
                i = terrain.width - 1;
                if (terrain.It(i, j).Value.Altitude == 1)
                {
                    Teleporters[Region.Village].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Forest });
                }
            }
            Entrances[Region.Plain].Add(Region.Village, new Vector2i(1, toPlain.Y * roomHeight));
            Entrances[Region.Forest].Add(Region.Village, new Vector2i(terrain.width - 2, toForest.Y * roomHeight));
            //var houseLocation = SetLonksHouse(terrain, TileType.None, 1);

            return terrain;
        }

        private SmartArray<Tile> _GenerateForest()
        {
            int region = (int)Region.Forest;
            int roomWidth = 12;
            int roomHeight = 12;
            int mazeWidth = (width[region]) / roomWidth;
            int mazeHeight = (height[region]) / roomHeight;

            SmartArray<Tile> terrain = new SmartArray<Tile>(mazeWidth * roomWidth, mazeHeight * roomHeight,
                (i, j) => new Tile { X = i, Y = j, Altitude = 1, Type = TileType.None });

            SmartArray<Room> rooms = new SmartArray<Room>(mazeWidth, mazeHeight,
                (i, j) => new Room(i + j * mazeWidth, i, j,
                j < mazeHeight - 1,
                j > 0,
                i > 0,
                i < mazeWidth - 1));
            List<KeyValuePair<Room, Room>> walls = new List<KeyValuePair<Room, Room>>();

            for (int i = 0; i < mazeWidth; i++)
            {
                for (int j = 0; j < mazeHeight; j++)
                {
                    var room = rooms.It(i, j);
                    if (room.Right.Value != null && room.Value.Y == room.Right.Value.Y)
                        walls.Add(new KeyValuePair<Room, Room>(room.Value, room.Right.Value));

                    if (room.Bottom.Value != null && room.Value.X == room.Bottom.Value.X)
                        walls.Add(new KeyValuePair<Room, Room>(room.Value, room.Bottom.Value));
                }
            }
            //Debug.Log("Rooms: " + mazeWidth.ToString() + "x" + mazeHeight.ToString());
            //Debug.Log("Walls: " + walls.Count.ToString());
            int removedWalls = 0;
            while (removedWalls < (mazeWidth * mazeHeight) - 1)
            {
                int iterator = uRandom.Range(0, walls.Count);
                var wall = walls[iterator];
                while (wall.Key.group == wall.Value.group) // while wall separates connected rooms
                {
                    iterator = (iterator + 1) % walls.Count;
                    wall = walls[iterator];
                }

                int groupA = wall.Key.group;
                int groupB = wall.Value.group;
                walls.Remove(wall);
                removedWalls++;
                for (int i = 0; i < mazeWidth; i++)
                {
                    for (int j = 0; j < mazeHeight; j++)
                    {
                        if (rooms.It(i, j).Value.group == groupB)
                            rooms.It(i, j).Value.group = groupA;
                    }
                }
            }

            foreach (var wall in walls)
            {
                if (wall.Key.X == wall.Value.X)
                {
                    if (wall.Key.Y > wall.Value.Y)
                    {
                        wall.Key.south = false;
                        wall.Value.north = false;
                    }
                    else
                    {
                        wall.Key.north = false;
                        wall.Value.south = false;
                    }
                }
                if (wall.Key.Y == wall.Value.Y)
                {
                    if (wall.Key.X > wall.Value.X)
                    {
                        wall.Key.west = false;
                        wall.Value.east = false;
                    }
                    else
                    {
                        wall.Key.east = false;
                        wall.Value.west = false;
                    }
                    var room = wall.Key;
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                    Debug.Log(" n=" + room.north.ToString() + " e=" + room.east.ToString() + " s=" + room.south.ToString() + " w=" + room.west.ToString());
                    room = wall.Value;
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                    Debug.Log(" n=" + room.north.ToString() + " e=" + room.east.ToString() + " s=" + room.south.ToString() + " w=" + room.west.ToString());
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                }
            }

            Vector2i toVillage = new Vector2i(0, uRandom.Range(0, mazeHeight));
            rooms.It(toVillage.X, toVillage.Y).Value.west = true;
            Vector2i toDungeon = new Vector2i(mazeWidth - 1, uRandom.Range(0, mazeHeight));
            rooms.It(toDungeon.X, toDungeon.Y).Value.east = true;

            ReadRoomPremades(rooms, terrain, ForestRoomPremades, Region.Forest, roomWidth, roomHeight);


            // should be replaced with a generic function
            for (int j = 0; j < terrain.height; j++)
            {
                int i = 0;
                if (terrain.It(0, j).Value.Type == TileType.Bush)
                    terrain.It(0, j).Value.Type = TileType.Grass;
                if (terrain.It(1, j).Value.Type == TileType.Bush)
                    terrain.It(1, j).Value.Type = TileType.Grass;
                if (terrain.It(i, j).Value.Type == TileType.Grass)
                {
                    Teleporters[Region.Forest].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Village });
                }
            }
            Entrances[Region.Village].Add(Region.Forest, new Vector2i(1, toVillage.Y * roomHeight + 6));

            // tmp
            for (int j = 0; j < terrain.height; j++)
            {
                int i = terrain.width - 1;
                if (terrain.It(0, j).Value.Type == TileType.Bush)
                    terrain.It(0, j).Value.Type = TileType.Grass;
                if (terrain.It(1, j).Value.Type == TileType.Bush)
                    terrain.It(1, j).Value.Type = TileType.Grass;
                if (terrain.It(i, j).Value.Type == TileType.Grass)
                {
                    Teleporters[Region.Forest].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Dungeon });
                }
            }
            Entrances[Region.Dungeon].Add(Region.Forest, new Vector2i(1, toDungeon.Y * roomHeight + 6));
            // end tmp

            return terrain;
        }

        private SmartArray<Tile> _GenerateDungeon()
        {
            int region = (int)Region.Dungeon;
            int roomWidth = 12;
            int roomHeight = 12;
            int mazeWidth = (width[region]) / roomWidth;
            int mazeHeight = (height[region]) / roomHeight;

            SmartArray<Tile> terrain = new SmartArray<Tile>(mazeWidth * roomWidth, mazeHeight * roomHeight,
                (i, j) => new Tile { X = i, Y = j, Altitude = 1, Type = TileType.None });

            SmartArray<Room> rooms = new SmartArray<Room>(mazeWidth, mazeHeight,
                (i, j) => new Room(i + j * mazeWidth, i, j,
                j < mazeHeight - 1,
                j > 0,
                i > 0,
                i < mazeWidth - 1));
            List<KeyValuePair<Room, Room>> walls = new List<KeyValuePair<Room, Room>>();

            // same  algorithm as the forest for now
            for (int i = 0; i < mazeWidth; i++)
            {
                for (int j = 0; j < mazeHeight; j++)
                {
                    var room = rooms.It(i, j);
                    if (room.Right.Value != null && room.Value.Y == room.Right.Value.Y)
                        walls.Add(new KeyValuePair<Room, Room>(room.Value, room.Right.Value));

                    if (room.Bottom.Value != null && room.Value.X == room.Bottom.Value.X)
                        walls.Add(new KeyValuePair<Room, Room>(room.Value, room.Bottom.Value));
                }
            }
            int removedWalls = 0;
            while (removedWalls < (mazeWidth * mazeHeight) - 1)
            {
                int iterator = uRandom.Range(0, walls.Count);
                var wall = walls[iterator];
                while (wall.Key.group == wall.Value.group) // while wall separates connected rooms
                {
                    iterator = (iterator + 1) % walls.Count;
                    wall = walls[iterator];
                }

                int groupA = wall.Key.group;
                int groupB = wall.Value.group;
                walls.Remove(wall);
                removedWalls++;
                for (int i = 0; i < mazeWidth; i++)
                {
                    for (int j = 0; j < mazeHeight; j++)
                    {
                        if (rooms.It(i, j).Value.group == groupB)
                            rooms.It(i, j).Value.group = groupA;
                    }
                }
            }

            foreach (var wall in walls)
            {
                if (wall.Key.X == wall.Value.X)
                {
                    if (wall.Key.Y > wall.Value.Y)
                    {
                        wall.Key.south = false;
                        wall.Value.north = false;
                    }
                    else
                    {
                        wall.Key.north = false;
                        wall.Value.south = false;
                    }
                }
                if (wall.Key.Y == wall.Value.Y)
                {
                    if (wall.Key.X > wall.Value.X)
                    {
                        wall.Key.west = false;
                        wall.Value.east = false;
                    }
                    else
                    {
                        wall.Key.east = false;
                        wall.Value.west = false;
                    }
                    var room = wall.Key;
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                    Debug.Log(" n=" + room.north.ToString() + " e=" + room.east.ToString() + " s=" + room.south.ToString() + " w=" + room.west.ToString());
                    room = wall.Value;
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                    Debug.Log(" n=" + room.north.ToString() + " e=" + room.east.ToString() + " s=" + room.south.ToString() + " w=" + room.west.ToString());
                    Debug.Log("WRoom " + room.X.ToString() + ";" + room.Y.ToString());
                }
            }

            Vector2i toForest = new Vector2i(0, uRandom.Range(0, mazeHeight));
            rooms.It(toForest.X, toForest.Y).Value.west = true;

            ReadRoomPremades(rooms, terrain, ForestRoomPremades, Region.Dungeon, roomWidth, roomHeight);
            SetContour(terrain, TileType.WallTop, TileType.WallBottom);


            // should be replaced with a generic function
            for (int j = 0; j < terrain.height; j++)
            {
                int i = 0;
                if (terrain.It(i, j).Value.Type == TileType.Slab || terrain.It(i, j).Value.Type == TileType.AncientSlab)
                {
                    Teleporters[Region.Dungeon].Add(new Teleporter { sourceTile = new Vector2i(i, j), destination = Region.Forest });
                }
            }
            Entrances[Region.Forest].Add(Region.Dungeon, new Vector2i(1, toForest.Y * roomHeight + 5));


            // testing
            Teleporters[Region.Plain].Add(new Teleporter { sourceTile = new Vector2i(playerStartingPosition.X, playerStartingPosition.Y + 1), destination = Region.Dungeon });
            Entrances[Region.Plain].Add(Region.Dungeon, new Vector2i(1, toForest.Y * roomHeight + 5));

            return terrain;
        }


        private void ReadRoomPremades(SmartArray<Room> rooms, SmartArray<Tile> terrain, Sprite[] premades, Region region, int roomWidth = 12, int roomHeight = 12)
        {
            foreach (Room room in rooms.ToArray())
            {
                Sprite sprite = premades[0];
                if (room.premadeIndex == -1)
                {
                    int choice = 0;
                    if (!room.north)
                        choice++;
                    choice *= 2;
                    if (!room.east)
                        choice++;
                    choice *= 2;
                    if (!room.south)
                        choice++;
                    choice *= 2;
                    if (!room.west)
                        choice++;
                    sprite = premades[choice];
                }
                else
                {
                    sprite = premades[room.premadeIndex];
                }

                int spriteX = uRandom.Range(0, (int)sprite.textureRect.width / roomWidth) + (int)sprite.textureRect.x;
                int spriteY = uRandom.Range(0, (int)sprite.textureRect.height / roomHeight) + (int)sprite.textureRect.y;
                for (int i = 0; i < roomWidth; i++)
                {
                    for (int j = 0; j < roomHeight; j++)
                    {
                        Tile t = terrain.It(room.X * roomWidth + i, room.Y * roomHeight + j).Value;
                        var pixel = sprite.texture.GetPixel(i + spriteX * roomWidth, j + spriteY * roomHeight);
                        if (EditorColors[region].ContainsKey(ColorToInt(pixel)))
                        {
                            t.Type = EditorColors[region][ColorToInt(pixel)];
                        }
                    }
                }
            }
        }


        private void CreepAltitude(SmartArray<Tile> terrain, int altitude, int startX, int startY)
        {
            Func<SmartArray<Tile>.Sentry, bool> Creep = null;
            Creep = new Func<SmartArray<Tile>.Sentry, bool>(it =>
            {
                if (it.Value == null || it.Value.Type == TileType.CliffTop || it.Value.Type == TileType.CliffBottom || it.Value.Parcoured)
                    return false;
                else
                {
                    it.Value.Altitude = altitude;
                    it.Value.Parcoured = true;
                    Creep(it.Top);
                    Creep(it.Bottom);
                    Creep(it.Left);
                    Creep(it.Right);
                    return true;
                }
            });
            var sentry = terrain.It(startX, startY);
            Creep(sentry);
            foreach (var item in terrain.ToArray())
            {
                item.Parcoured = false;
            }
        }

        private void CreepAltitude2(SmartArray<Tile> terrain, int altitude, int startX, int startY)
        {
            Func<Tile, bool> Creep = null;
            Creep = new Func<Tile, bool>(it =>
            {
                if (it == null || it.Type == TileType.CliffTop || it.Type == TileType.CliffBottom || it.Parcoured)
                    return false;
                else
                {
                    it.Altitude = altitude;
                    it.Parcoured = true;
                    if (it.X > 1)
                        Creep(terrain.It(it.X - 1, it.Y).Value);
                    if (it.X < terrain.width - 1)
                        Creep(terrain.It(it.X + 1, it.Y).Value);
                    if (it.Y > 1)
                        Creep(terrain.It(it.X, it.Y - 1).Value);
                    if (it.Y < terrain.height - 1)
                        Creep(terrain.It(it.X, it.Y + 1).Value);
                    return true;
                }
            });
            var sentry = terrain.It(startX, startY);
            Creep(sentry.Value);
            foreach (var item in terrain.ToArray())
            {
                item.Parcoured = false;
            }
        }

        private void _AddTeleporters(SmartArray<Tile> terrain, Region current, Region link)
        {
            Vector2i connexion = new Vector2i(0, 0);
            if (uRandom.value > 0.5f)
            {
                connexion.X = uRandom.Range(terrain.width / 4, ((terrain.width - 5) * 3) / 4);
                if (uRandom.value > 0.5f)
                {
                    connexion.Y = terrain.height - 1;
                    for (int i = connexion.X; i < connexion.X + 5; i++)
                    {
                        Teleporters[current].Add(new Teleporter { sourceTile = new Vector2i(i, connexion.Y), destination = link });
                        Debug.Log("add teleporter @ " + i + " " + connexion.Y);
                        for (int j = connexion.Y; j > terrain.height / 2; j--)
                        {
                            terrain.It(i, j).Value.Altitude = 1;
                        }
                    }
                    Entrances[link].Add(current, new Vector2i(connexion.X + 2, connexion.Y - 1));
                }
                else
                {
                    for (int i = connexion.X; i < connexion.X + 5; i++)
                    {
                        Teleporters[current].Add(new Teleporter { sourceTile = new Vector2i(i, connexion.Y), destination = link });
                        Debug.Log("add teleporter @ " + i + " " + connexion.Y);
                        for (int j = connexion.Y; j < terrain.height / 2; j++)
                        {
                            terrain.It(i, j).Value.Altitude = 1;
                        }
                    }
                    Entrances[link].Add(current, new Vector2i(connexion.X + 2, connexion.Y + 1));
                }
            }
            else
            {
                connexion.Y = uRandom.Range(terrain.height / 4, ((terrain.height - 5) * 3) / 4);
                if (uRandom.value > 0.5f)
                {
                    connexion.X = terrain.width - 1;
                    for (int j = connexion.Y; j < connexion.Y + 5; j++)
                    {
                        Teleporters[current].Add(new Teleporter { sourceTile = new Vector2i(connexion.X, j), destination = link });
                        Debug.Log("add teleporter @ " + connexion.X + " " + j);
                        for (int i = connexion.X; i > terrain.width / 2; i--)
                        {
                            terrain.It(i, j).Value.Altitude = 1;
                        }
                    }
                    Entrances[link].Add(current, new Vector2i(connexion.X - 1, connexion.Y + 2));
                }
                else
                {
                    for (int j = connexion.Y; j < connexion.Y + 5; j++)
                    {
                        Teleporters[current].Add(new Teleporter { sourceTile = new Vector2i(connexion.X, j), destination = link });
                        Debug.Log("add teleporter @ " + connexion.X + " " + j);
                        for (int i = connexion.X; i < terrain.width / 2; i++)
                        {
                            terrain.It(i, j).Value.Altitude = 1;
                        }
                    }
                    Entrances[link].Add(current, new Vector2i(connexion.X + 1, connexion.Y + 2));
                }
            }
        }

        private List<Monster> SpawnRandomMonsters(Map map, MonsterType type, int number = 1, TileType idealType = TileType.Grass, int idealAltitude = -1)
        {
            var monsters = new List<Monster>();
            var locations = SpawnLocations(map.Tiles, idealType, idealAltitude);
            for (int i = 0; i < number && locations.Count > 0; i++)
            {
                Tile location = locations[uRandom.Range(0, locations.Count)];
                monsters.Add(new Monster(location.X, location.Y, type));
                locations.Remove(location);
            }

            return monsters;
        }

        public static List<Tile> SpawnLocations(SmartArray<Tile> terrain, TileType idealType, int idealAltitude = -1)
        {
            var locations = new List<Tile>();
            for (int i = 0; i < terrain.width; i++)
            {
                for (int j = 0; j < terrain.height; j++)
                {
                    Tile thisTile = terrain.It(i, j).Value;
                    if (thisTile != null && !thisTile.HasCollision && thisTile.Type == idealType
                        && (idealAltitude == -1 || thisTile.Altitude == idealAltitude))
                        locations.Add(thisTile);
                }
            }
            return locations;
        }

        private void AddTrees(SmartArray<Tile> terrain, int number = 1, int idealAltitude = -1)
        {
            for (int tree = 0; tree < number; tree++)
            {
                if (PutTree(terrain, TileType.None, idealAltitude) == null)
                    break;
            }
        }

        private void AddRocks(SmartArray<Tile> terrain, int number = 1, int idealAltitude = -1)
        {
            var locations = SpawnLocations(terrain, TileType.None, idealAltitude);
            for (int rock = 0; rock < number && locations.Count > 0; rock++)
            {
                Tile location = locations[uRandom.Range(0, locations.Count)];
                location.Type = TileType.Rock;
                locations.Remove(location);
            }
        }

        private void GenerateCircle(SmartArray<Tile> terrain, float x_center, float y_center, float radius)
        {
            for (int i = 1; i < terrain.width - 1; i++)
            {
                for (int j = 2; j < terrain.height - 2; j++)
                {
                    if (Mathf.Sqrt(Mathf.Pow(i - x_center, 2) + Mathf.Pow(j - y_center, 2)) < radius)
                    {
                        terrain.It(i, j).Value.Altitude = 1;
                    }
                }
            }
        }

        private SmartArray<Tile>.Sentry SetLonksHouse(SmartArray<Tile> terrain, TileType idealType, float idealAltitude = -1)
        {
            var pick = PutHouse(terrain, idealType, idealAltitude);
            if (pick == null)
                return null;
            playerStartingPosition = new Vector2i((int)pick.Value.X + 2, (int)pick.Value.Y - 1); // set the player starting position in front of the house
            return pick;
        }

        private SmartArray<Tile>.Sentry PutHouse(SmartArray<Tile> terrain, TileType idealType = TileType.None, float idealAltitude = -1)
        {
            var pick = Build(terrain, 6, 7, TileType.House, TileType.BuiltGrass, new Vector2(0, 2), idealType, idealAltitude);
            for (int w = 0; w < 6; w++)
            {
                for (int h = 0; h < 2; h++)
                {
                    terrain.It(pick.Value.X + w, pick.Value.Y + h).Value.Type = TileType.Grass;
                }
            }
            return pick;
        }

        private SmartArray<Tile>.Sentry PutTree(SmartArray<Tile> terrain, TileType idealType = TileType.None, float idealAltitude = -1)
        {
            return Build(terrain, 4, 4, TileType.Tree, TileType.BuiltGrass, Vector2.zero, idealType, idealAltitude);
        }

        private SmartArray<Tile>.Sentry Build(SmartArray<Tile> terrain, int buildWidth, int buildHeight, TileType building, TileType filler,
            Vector2 buildingOffset, TileType idealType = TileType.None, float idealAltitude = -1)
        {
            List<SmartArray<Tile>.Sentry> possibleLocations = new List<SmartArray<Tile>.Sentry>();

            // search for places where a 4x4 terrain is constructible
            for (int i = 0; i < terrain.width - buildWidth + 1; i++)
            {
                for (int j = 0; j < terrain.height - buildHeight + 1; j++)
                {
                    bool valid = true;
                    for (int w = 0; w < buildWidth; w++)
                    {
                        for (int h = 0; h < buildHeight; h++)
                        {
                            if ((idealAltitude > -1 && Mathf.Abs(terrain.It(i + w, j + h).Value.Altitude - idealAltitude) > 0.01f)
                                || terrain.It(i + w, j + h).Value.Type != idealType)
                            {
                                valid = false;
                                break;
                            }
                        }
                        if (!valid)
                            break;
                    }
                    if (valid)
                        possibleLocations.Add(terrain.It(i, j));
                }
            }
            if (possibleLocations.Count < 1)
                return null;

            // select random location among valid ones
            var pick = possibleLocations[uRandom.Range(0, possibleLocations.Count)];
            // fill in as Grass to make it not None
            // otherwise, it could get filled with other builds
            for (int w = 0; w < buildWidth; w++)
            {
                for (int h = 0; h < buildHeight; h++)
                {
                    terrain.It(pick.Value.X + w, pick.Value.Y + h).Value.Type = filler;
                }
            }

            for (int i = 0; i < buildingOffset.x; i++)
                pick = pick.Left;
            for (int i = 0; i > buildingOffset.x; i--)
                pick = pick.Right;
            for (int i = 0; i < buildingOffset.y; i++)
                pick = pick.Top;
            for (int i = 0; i > buildingOffset.y; i--)
                pick = pick.Bottom;
            pick.Value.Type = building; // set the tree location
            return pick;
        }

        private void AddCliffs(SmartArray<Tile> terrain, int altitude)
        {
            var High = new Func<SmartArray<Tile>.Sentry, bool>(it =>
            {
                if (it.Value == null)
                    return false;
                else
                    return it.Value.Altitude > altitude - 0.01f;
            });


            for (int i = 0; i < terrain.width; i++)
            {
                for (int j = 0; j < terrain.height; j++)
                {
                    var it = terrain.It(i, j);
                    if (it.Value.Altitude == 0)
                    {
                        if (High(it.Top) || High(it.Top.Left) || High(it.Top.Right) || High(it.Right) || High(it.Left)
                            || High(it.Bottom) || High(it.Bottom.Left) || High(it.Bottom.Right)
                            || High(it.Top.Top) || High(it.Top.Top.Left) || High(it.Top.Top.Right))
                        {
                            it.Value.Type = TileType.CliffTop;
                            it.Value.Altitude = 0.5f;
                        }
                    }
                }
            }
            CompleteCliffs(terrain, 0);
        }

        private void CompleteCliffs(SmartArray<Tile> terrain, int altitude)
        {
            var IsCliff = new Func<SmartArray<Tile>.Sentry, bool>(i =>
            {
                return i.Value != null && i.Value.Type == TileType.CliffTop;
            });
            for (int i = 0; i < terrain.width; i++)
            {
                for (int j = 0; j < terrain.height; j++)
                {
                    var it = terrain.It(i, j);
                    if (it.Value.Altitude == altitude && (IsCliff(it.Top) || IsCliff(it.Bottom) || IsCliff(it.Left) || IsCliff(it.Right)
                         || IsCliff(it.Top.Right) || IsCliff(it.Bottom.Left) || IsCliff(it.Left.Top) || IsCliff(it.Right.Bottom)))
                    {
                        it.Value.Type = TileType.CliffBottom;
                        it.Value.Altitude = 0.5f;
                    }
                }
            }
        }

        private void FillVoid(SmartArray<Tile> terrain, TileType filler)
        {
            for (int i = 0; i < terrain.width; i++)
            {
                for (int j = 0; j < terrain.height; j++)
                {
                    if (terrain.It(i, j).Value.Type == TileType.None)
                        terrain.It(i, j).Value.Type = filler;
                }
            }
        }

        private void SetContour(SmartArray<Tile> terrain, TileType inside, TileType liner)
        {
            var IsOutside = new Func<SmartArray<Tile>.Sentry, bool>(i =>
            {
                return i.Value != null && i.Value.Type != inside && i.Value.Type != liner;
            });
            for (int i = 0; i < terrain.width; i++)
            {
                for (int j = 0; j < terrain.height; j++)
                {
                    var it = terrain.It(i, j);
                    if (it.Value.Type == inside && (IsOutside(it.Top.Left) || IsOutside(it.Top.Right) || IsOutside(it.Bottom.Left) || IsOutside(it.Bottom.Right)))
                    {
                        it.Value.Type = liner;
                    }
                }
            }
        }
    }
}
