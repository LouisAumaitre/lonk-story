namespace LonkStory
{
    public enum MonsterType
    {
        SLIME,
        BAT,
        NUT_SLINGER,
        TURTLE,
        SKULL
    }
}