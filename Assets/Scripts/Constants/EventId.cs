namespace LonkStory.Constants
{
    public enum EventId
    {
        Transition,
        PlayerChangedTile
    }
}
