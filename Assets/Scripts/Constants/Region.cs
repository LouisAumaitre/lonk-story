namespace LonkStory.Constants
{
    public enum Region
    {
        Plain,
        Village,
        Forest,
        Dungeon,
        NumberOfRegions // keep at the end of the enum
    }
}
