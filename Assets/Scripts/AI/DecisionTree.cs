using System;
using System.Collections;
using System.Collections.Generic;
using LonkStory.Constants;
using UnityEngine;

namespace LonkStory.AI
{
    public class DecisionTree
    {
        private DecisionNode _root;
        private DecisionState _state;
        private MonoBehaviour _owner;

        public bool IsWorking { get { return _state == DecisionState.WORKING; } }

        public DecisionTree(MonoBehaviour owner, DecisionNode root)
        {
            _owner = owner;
            _state = DecisionState.IDLE;
            _root = root;
        }

        public void Next()
        {
            if (_root == null)
                return;

            _state = DecisionState.WORKING;
            _Next(_root);
        }

        private IEnumerator _Do(DecisionNode leaf)
        {
            yield return leaf.Do(() => { _state = DecisionState.IDLE; });
        }

        private void _Next(DecisionNode node)
        {
            if (node.IsLeaf)
            {
                Debug.Log("StartCoroutine");
                _owner.StartCoroutine(_Do(node));
            }
            else
            {
                foreach (DecisionNode child in node.Children)
                {
                    if (child.IsTrue())
                    {
                        _Next(child);
                        return;
                    }
                }
            }
        }
    }
}