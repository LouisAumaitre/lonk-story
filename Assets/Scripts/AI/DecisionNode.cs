using System;
using System.Collections;
using System.Collections.Generic;

namespace LonkStory.AI {

        public class DecisionNode
        {
            private Func<bool> _condition;
            private Func<IEnumerator> _do;

            private List<DecisionNode> _children;
            public bool IsLeaf { get { return _children.Count == 0; } }
            public List<DecisionNode> Children { get { return _children; } }

            public DecisionNode() {
                _condition = () => true;
                _children = new List<DecisionNode>();
                _do = null;
            }

            public DecisionNode(Func<bool> condition) {
                _condition = condition;
                _children = new List<DecisionNode>();
                _do = null;
            }

            public DecisionNode(Func<bool> condition, Func<IEnumerator> doSomething)
            {
                _condition = condition;
                _children = new List<DecisionNode>();
                _do = doSomething;
            }

            public DecisionNode Add(DecisionNode node)
            {
                _children.Add(node);
                return this;
            }

            public IEnumerator Do(Action atLast)
            {
                if (_do != null)
                    yield return _do();

                if (atLast != null)
                    atLast();
            }

            public bool IsTrue()
            {
                return _condition == null ? false : _condition.Invoke();
            }
        }
}