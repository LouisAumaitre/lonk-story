using LonkStory.Players;
using LonkStory.Storage;
using LonkStory.Utilities;
using UnityEngine;
using UnityEngine.UI;

namespace LonkStory.UI.Managers
{
    public class UIHeartManager : MonoBehaviour
    {
        public Transform hearts;
        public GameObject heartPrefab;

        public Sprite empty;
        public Sprite oneThird;
        public Sprite twoThird;
        public Sprite full;

        private void _WatchPlayerLife(object nextValue)
        {
            Player player = (Player)nextValue;
            _UpdateLife(player.life, player.maxLife);
        }

        void Awake()
        {
            Binding.WatchValue("playerLife", _WatchPlayerLife);
        }

        void OnDestroy()
        {
            Binding.UnwatchValue("playerLife", _WatchPlayerLife);    
        }

        void Start()
        {
            _UpdateLife(Store.player.life, Store.player.maxLife);
        }

        void _AssignSprite(Image image, int life)
        {
            if (life >= 3)
            {
                image.sprite = full;
            }
            else if (life == 2)
            {
                image.sprite = twoThird;
            }
            else if (life == 1)
            {
                image.sprite = oneThird;
            }
            else if (life == 0)
            {
                image.sprite = empty;
            }
            else
            {
                // Detect errors. This case should be impossible.
                image.color = Color.yellow;
            }
        }

        void _UpdateLife(int life, int maxLife)
        {
            foreach (Transform child in hearts)
            {
                Destroy(child.gameObject);
            }

            while (maxLife > 0)
            {
                GameObject heart = Instantiate(heartPrefab, hearts);
                _AssignSprite(heart.GetComponent<Image>(), life);
                maxLife -= 1;
                life = life - 3 <= 0 ? 0 : life - 3;
            }
        }
    }
}